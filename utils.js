const createHash = require('create-hash');

module.exports = {

    sha256 : function(buffer) {
        return createHash('sha256')
            .update(buffer)
            .digest();
    },

    doublesha: function(buffer) {
        return module.exports.sha256(module.exports.sha256(buffer));
    },

    calculateTarget : function(bits){ //ret : Buffer 
        const exponent = ((bits & 0xff000000) >> 24) - 3;
        const mantissa = bits & 0x007fffff;
        const target = Buffer.alloc(32, 0);
        target.writeUIntBE(mantissa, 29 - exponent, 3);
        return target;
    },

    reverseBuffer : function (buffer) {
        if (buffer.length < 1) return buffer;
        var j = buffer.length - 1;
        var tmp = 0;
        for (var i = 0; i < buffer.length / 2; i++) {
            tmp = buffer[i];
            buffer[i] = buffer[j];
            buffer[j] = tmp;
            j--;
        }
        return buffer;
    }
}