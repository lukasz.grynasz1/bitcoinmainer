const client = require('stratum-client');
var utils = require("./utils.js");


const Client = client({
  server: "grlcgang.com",
  port: 3333,
  worker: "KorkyMonster.testing",
  password: "x",
  autoReconnectOnError: true,
  onConnect: () => console.log('Connected to server'),
  onClose: () => console.log('Connection closed'),
  onError: (error) => console.log('Error', error.message),
  onAuthorizeSuccess: () => console.log('Worker authorized'),
  onAuthorizeFail: () => console.log('WORKER FAILED TO AUTHORIZE OH NOOOOOO'),
  onNewDifficulty: (newDiff) => console.log('New difficulty', newDiff),
  onSubscribe: (subscribeData) => console.log('[Subscribe]', subscribeData),

  //handling new mining job
  onNewMiningWork: (newWork) => handleJob(newWork),

  onSubmitWorkSuccess: (error, result) => console.log("Yay! Our work was accepted!"),
  onSubmitWorkFail: (error, result) => console.log("Oh no! Our work was refused because: " + error),
});


function handleJob(workObject) {
  
    var targetBuf = utils.calculateTarget(parseInt("0x" +workObject.nbits));

    
    var shouldBrake = false;
    var maxNonce = Math.pow(2, 32);
    //zaczynamy kopanie 
    for(var i = 0;i<maxNonce && !shouldBrake;i++) {
  
        //tu można zaczynać od jakies randomowej wartosci 
      var extraNonce = Buffer.alloc(workObject.extraNonce2Size);
      extraNonce.writeUInt32BE(i ,0);

      var coinbase = buildCoinbaseTxHash(workObject, extraNonce.toString('hex'));
      var merkleRoot = buildMerkleRoot(coinbase, workObject.merkle_branch);
      
      for(var nonce=0;nonce<maxNonce;nonce++) {
          
        var buf = Buffer.alloc(4);
        buf.writeUInt32BE(nonce,0)

        var nonceHex = buf.toString('hex');

        var header = buildHeader(nonceHex, workObject, utils.reverseBuffer(merkleRoot).toString('hex'));
          
        var headerHash = utils.doublesha(Buffer.from(header, "hex"));
        console.log(i,nonceHex, headerHash.toString('hex'));

        if(Buffer.compare(headerHash, targetBuf) == -1) {
          console.log("Hash found ExtraNonce : " + extraNonce.toString('hex')," Nonce : " + nonceHex);
          shouldBrake = true;
          break;
        }
      
      }
    }
}

//return sha256(sha256(coinbase));
function buildCoinbaseTxHash(workObject, extraNonce2Hex) {
  var coinbase = workObject.coinb1 + workObject.extraNonce1 + extraNonce2Hex + workObject.coinb2;
  return utils.doublesha(Buffer.from(coinbase, "hex"));
}

function buildMerkleRoot(coinbaseHashBin, merkleBranch) {
  var merkleRoot = coinbaseHashBin

  for(var h in merkleBranch) {
    var concatBuf = Buffer.concat([merkleRoot, Buffer.from(h, "hex")])
    merkleRoot = utils.doublesha(concatBuf);
  }
          
  return merkleRoot.toString('hex');
}

function buildHeader(nonce, workObject, merkleRoot) {
  return workObject.version + workObject.prevhash + merkleRoot + workObject.ntime + workObject.nbits + nonce +
  '000000800000000000000000000000000000000000000000000000000000000000000000000000000000000080020000';


}